import 'select2';
import 'slick-carousel';
import 'magnific-popup';

import './libs/hamburger_menu.js';
import './libs/artmedia_functions.js';
import './libs/additional_functions.js';
import './libs/plugin_parameters.js';
//import './libs/tree_menu.js';

import $ from 'jquery';
window.jQuery = $;
window.$ = $;

$(document).ready(function(){
	$('div.fixed_link a').each((i, e) => {
		$(e).hover(function (event){
			// console.log($(e).siblings('h3'))
			$(e).siblings('h3').fadeToggle('fast');
		});
	});
});