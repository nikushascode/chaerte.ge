<!doctype html>
<html lang="">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Mindfield</title>
<link rel="shortcut icon" href="<?php // echo base_url(); ?>themes/images/favicon.png">
<meta name="theme-color" content="#01579b">
<meta name="msapplication-navbutton-color" content="#01579b">
<meta name="apple-mobile-web-app-status-bar-style" content="#01579b">
<link rel="stylesheet" href="scripts/css/main.css">
<script type="text/javascript" src="scripts/js/main.js"></script>
</head>
<body>

<section class="login_wrap trans-all-4">
	<div class="login">
		<div class="logo_lang">
			<div class="logo">
				<svg viewBox="0 0 172 131">
					<g>
						<g>
							<g>
								<path d="M86.4,46V1.1"></path>
								<path d="M97.9,2.2l-11.5,9.7"></path>
								<path d="M125.9,23.1L106,34.5H86.4"></path>
								<path d="M130.6,34.5l-14.2-6"></path>
								<path d="M109,6.5l-5.5,9.5l2.5,18.5"></path>
								<path d="M118.3,13.6L103.5,16"></path>
								<path d="M63.7,7l5.1,9l17.6,7.1"></path>
								<path d="M74.2,2.2L68.8,16"></path>
							</g>
							<g>
								<path d="M86.4,46l38.9,22.5"></path>
								<path d="M118.6,78L116,63.1"></path>
								<path d="M86.5,91.7l0.1-22.9l9.8-17"></path>
								<path d="M74.3,90.1l12.2-9.3"></path>
								<path d="M109.3,85.4l-5.4-9.5l-17.3-7.1"></path>
								<path d="M98.5,89.9l5.4-14"></path>
								<path d="M131.5,45.9l-10.3-0.1l-15,11.7"></path>
								<path d="M130.4,57.4l-9.2-11.6"></path>
							</g>
							<g>
								<path d="M86.4,46L47.5,68.5"></path>
								<path d="M42.6,58l14.2,5.1"></path>
								<path d="M46.8,23.3l19.7,11.5l9.8,17"></path>
								<path d="M54.2,13.5l2,15.3"></path>
								<path d="M40.9,46.2l10.9,0.1l14.7-11.5"></path>
								<path d="M42.3,34.6l9.5,11.7"></path>
								<path d="M64,85.2l5.1-8.9l-2.6-18.8"></path>
								<path d="M54.5,78.5l14.6-2.2"></path>
							</g>
						</g>
					</g>
					<g>
						<g>
							<path d="M12.5,130.5v-11c0-3.2-2.6-5.7-5.7-5.7h0c-3.2,0-5.7,2.6-5.7,5.7v11"></path>
							<path d="M23.9,130.5v-11c0-3.2-2.6-5.7-5.7-5.7h0c-3.2,0-5.7,2.6-5.7,5.7v11"></path>
						</g>
						<g>
							<path d="M34.5,130.5L34.5,113.8"></path>
							<path d="M34.5,110.3L34.5,110.3"></path>
						</g>
						<path d="M45,130.5v-8.4c0-4.6,3.7-8.4,8.4-8.4s8.4,3.7,8.4,8.4v8.4"></path>
						<g>
							<path d="M69.69999999999999,122.2A8.4,8.4 0,1,1 86.5,122.2A8.4,8.4 0,1,1 69.69999999999999,122.2"></path>
							<path d="M86.4,130.5L86.4,105"></path>
						</g>
						<g>
							<path d="M94.4,113.4c0-4.6,3.7-8.4,8.4-8.4"></path>
							<path d="M94.4,130.5L94.4,112.9"></path>
							<path d="M94.4,122.2c0-4.6,3.7-8.4,8.4-8.4"></path>
						</g>
						<g>
							<path d="M111.1,130.5L111.1,113.8"></path>
							<path d="M111.1,110.3L111.1,110.3"></path>
						</g>
						<g>
							<path d="M133.7,128.6c-1.4,1.2-3.3,1.9-5.3,1.9c-4.6,0-8.4-3.7-8.4-8.4c0-4.6,3.7-8.4,8.4-8.4c4.6,0,8.4,3.7,8.4,8.4"></path>
							<path d="M136.7,122.1L120.3,122.1"></path>
						</g>
						<path d="M146.3,130.5L146.3,105"></path>
						<g>
							<path d="M154.2,122.2A8.4,8.4 0,1,1 171,122.2A8.4,8.4 0,1,1 154.2,122.2"></path>
							<path d="M171,130.5L171,105"></path>
						</g>
					</g>
				</svg>
			</div>
			<div class="lang">
				<ul>
					<li class="active">
						<a href="#" title="Georgian">Ge</a>
					</li>
					<li>
						<a href="#" title="English">En</a>
					</li>
				</ul>
			</div>
			<div class="artmedia">
				<span>product of</span>
				<a href="http://artmedia.ge" target="_blank" title="Artmedia">
					<svg class="nosvg">
						<g>
							<path d="M1.3,13.4c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5S1.3,16.4,1.3,13.4"></path>
							<path d="M12.3,7.9v11"></path>
							<path d="M18.3,7.9v11"></path>
							<path d="M18.3,11.9c0-2.2,1.8-4,4-4s4,1.8,4,4"></path>
							<path d="M36.3,18.9c-2.2,0-4-1.8-4-4v-14"></path>
							<path d="M35.3,7.9h-6"></path>
							<path d="M40.3,7.9v11"></path>
							<path d="M48.3,11.9c0-2.2,1.8-4,4-4s4,1.8,4,4v7"></path>
							<path d="M40.3,11.9c0-2.2,1.8-4,4-4s4,1.8,4,4v5"></path>
							<path d="M69.3,17.6c-1,0.8-2.2,1.3-3.5,1.3c-3,0-5.5-2.5-5.5-5.5s2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5h-11"></path>
							<path d="M75.1,13.4c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5S75.1,16.4,75.1,13.4"></path>
							<path d="M86.1,0.9v18"></path>
							<path d="M90.9,7.9v11"></path>
							<path d="M90.9,0.9v2"></path>
							<path d="M95.5,13.4c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5S95.5,16.4,95.5,13.4"></path>
							<path d="M106.5,7.9v11"></path>
						</g>
					</svg>
				</a>
			</div>
		</div>
		<div class="login_form">
			<form action="login.php" autocomplete="off">
				<div class="form_item">
					<label for="form1">მომხმარებელი<sup>*</sup></label>
					<input type="text" id="form1" autofocus>
					<div class="error_message">ველი ცარიელია</div>
				</div>
				<div class="form_item">
					<label for="form2">პაროლი<sup>*</sup></label>
					<input type="password" id="form2">
					<div class="error_message">ველი ცარიელია</div>
				</div>
				<div class="form_item form_submit">
					<button class="gilaki">შესვლა</button>
				</div>
			</form>
		</div>
	</div>
</section>

</body>
</html>
