<!-- რესპონსივ მენიუ -->
<div class="resp_menu">
	<div class="resp_menu_toggle">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<div class="resp_menu_ul"></div>
</div>
<!-- რესპონსივ მენიუ -->
<!-- ლოგო -->
<div class="logo">
	<a href="#" title="მზა ტეგები">
		<img src="themes/images/logo.svg" alt="მზა ტეგები">
	</a>
	<h3>სლოგანი</h3>
</div>
<!-- ლოგო -->
<!-- ძიება -->
<div class="search">
	<form action="index.php" autocomplete="off">
		<input type="search" placeholder="ძიება">
		<button type="submit">
			<em class="ri-search-line"></em>
		</button>
	</form>
</div>
<div class="search_trigger">
	<a href="javascript:void(0);" title="ძიება">
		<em class="ri-search-line"></em>
	</a>
</div>
<div class="modal_search">
	<div class="close"></div>
	<div class="search_wrap">
		<div class="search_overlay"></div>
		<form action="index.php" autocomplete="off">
			<input type="search" placeholder="ძიება">
			<button type="submit">
				<em class="ri-search-line"></em>
			</button>
		</form>
	</div>
</div>
<!-- ძიება -->
<!-- ენა -->
<div class="lang">
	<ul>
		<li class="active">
			<a href="#" title="Georgian">Geo</a>
		</li>
		<li>
			<a href="#" title="English">Eng</a>
		</li>
		<li>
			<a href="#" title="Russian">Rus</a>
		</li>
	</ul>
</div>
<!-- ენა -->
<!-- სოციალური ქსელები -->
<div class="socials">
	<ul>
		<li>
			<a href="https://facebook.com" title="Facebook" target="_blank">
				<em class="ri-facebook-line"></em>
			</a>
		</li>
		<li>
			<a href="https://instagram.com" title="Instagram" target="_blank">
				<em class="ri-instagram-line"></em>
			</a>
		</li>
		<li>
			<a href="https://youtube.com" title="YouTube" target="_blank">
				<em class="ri-youtube-line"></em>
			</a>
		</li>
	</ul>
</div>
<!-- სოციალური ქსელები -->
<!-- ყველას ნახვა -->
<div class="see_all">
	<a href="#" title="ყველას ნახვა">ყველას ნახვა</a>
</div>
<!-- ყველას ნახვა -->
<!-- ბრედქრამბები -->
<div class="breadcrumbs">
	<ul>
		<li>
			<a href="#" title="მთავარი">მთავარი</a>
		</li>
		<li>
			<a href="#" title="ჩვენ შესახებ">ჩვენ შესახებ</a>
		</li>
		<li>ისტორია</li>
	</ul>
</div>
<!-- ბრედქრამბები -->
<!-- გვერდების გადამრთველი -->
<div class="pagination">
	<ul class="desktop_pagination">
		<li>
			<a href="#" title="პირველი">
				<em class="ri-skip-back-line"></em>
			</a>
		</li>
		<li>
			<a href="#" title="წინა">
				<em class="ri-arrow-left-s-line"></em>
			</a>
		</li>
		<li class="active">
			<a href="#" title="გვერდი 1">1</a>
		</li>
		<li>
			<a href="#" title="გვერდი 2">2</a>
		</li>
		<li>
			<a href="#" title="გვერდი 3">3</a>
		</li>
		<li>
			<a href="#" title="გვერდი 4">4</a>
		</li>
		<li>
			<a href="#" title="გვერდი 5">5</a>
		</li>
		<li>
			<a href="#" title="...">...</a>
		</li>
		<li>
			<a href="#" title="გვერდი 94">94</a>
		</li>
		<li>
			<a href="#" title="შემდეგი">
				<em class="ri-arrow-right-s-line"></em>
			</a>
		</li>
		<li>
			<a href="#" title="ბოლო">
				<em class="ri-skip-forward-line"></em>
			</a>
		</li>
	</ul>
	<ul class="device_pagination">
		<li>
			<a href="#" title="წინა">
				<em class="ri-arrow-left-s-line"></em>
			</a>
		</li>
		<li>
			<a href="#" title="შემდეგი">
				<em class="ri-arrow-right-s-line"></em>
			</a>
		</li>
	</ul>
</div>
<!-- გვერდების გადამრთველი -->
<!-- Rights/Artmedia -->
<div class="rights">&copy; <?php if(date("Y")!=="2020"){echo "2020 - ".date("Y");}else{echo date("Y");} ?> - კომპანია - ყველა უფლება დაცულია</div>
<div class="artmedia">
	<a href="http://artmedia.ge" target="_blank" title="Artmedia">
		<svg class="nosvg">
			<g>
				<path d="M19.8,0.7c3.9,0,7.1,0.4,9.7,1.3c2.6,0.9,4.7,2.1,6.2,3.8c1.6,1.6,2.7,3.7,3.3,6c0.6,2.4,1,5,1,7.9v4.1V47v2.3H20 c-3,0-5.7-0.3-8.2-0.8c-2.4-0.5-4.5-1.4-6.3-2.6c-1.8-1.2-3.1-2.8-4.1-4.7C0.5,39.3,0,36.9,0,34.1c0-2.4,0.4-4.8,1.6-6.9 c1.1-1.9,2.5-3.4,4.4-4.5c1.8-1.1,3.9-1.9,6.3-2.4c2.4-0.5,4.8-0.8,7.3-0.8c1.7,0,3.2,0.1,4.5,0.2c1.3,0.1,2.4,0.3,3.2,0.6V19 c0-2.2-0.7-4-2-5.4c-1.3-1.3-3.6-2-6.8-2l0-0.1c0.1-4.2-2.2-8.2-6-10.2l-0.2-0.1l0,0c0.9-0.1,1.9-0.2,2.9-0.3 C16.7,0.7,18.2,0.7,19.8,0.7"></path>
				<path class="eye" d="M7.1,14.5c1.7,0,3.1-1.4,3.1-3.1c0-1.7-1.4-3.1-3.1-3.1C5.3,8.2,4,9.6,4,11.3C4,13.1,5.3,14.5,7.1,14.5"></path>
			</g>
		</svg>
	</a>
</div>
<!-- Rights/Artmedia -->