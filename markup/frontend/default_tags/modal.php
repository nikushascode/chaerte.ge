<div class="modal_overlay"></div>
<div class="modal">
	<div class="close">
		<em class="ri-close-line"></em>
	</div>
	<div class="modal_head">
		<h3>მოდალის სათაურის ტექსტი</h3>
	</div>
	<div class="modal_body">
		<p>ეს არის საცდელი ტექსტი, რომელიც მოცემულ შემთხვევაში გამოიყენება როგორც მაგალითი. მისი საშუალებით ჩვენ ვხვდებით თუ როგორი იქნება მომავალში საიტის ტექსტით დაფარული არე დიზაინერულ ასპექტში. იგი გვეხმარება უკეთ აღვიქვათ საიტის ერთიანი დიზაინი, რაც მომავალში გამორიცხავს მსგავსი პრობლემების წამოჭრას. ეს ტექსტი შესაძლოა გამოყენებულ იქნას საიტის დიზაინის შექმნის ნებისმიერ სტადიაში მაგ. საიტის დიზაინის რომელიმე გრაფიკულ პროგრამაში აწყობის დროს.</p>
	</div>
</div>

<div class="prompt_overlay"></div>
<div class="prompt">
	<div class="modal_head">
		<h3>დარწმუნებული ხართ რომ გსურთ წაშლა?</h3>
	</div>
	<div class="modal_body">
		<div class="prompt_actions">
			<a href="javascript:void(0);" title="დიახ" class="gilaki">დიახ</a>
			<a href="javascript:void(0);" title="არა" class="gilaki empty">არა</a>
		</div>
	</div>
</div>