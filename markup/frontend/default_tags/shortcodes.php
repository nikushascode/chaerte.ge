<!-- Blockquote -->
<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci dignissimos explicabo, officiis saepe, deleniti vitae, voluptatem sit rem, quasi quos voluptatibus dolorem in rerum quaerat ipsa quo vel! Necessitatibus, dolore.</blockquote>
<!-- Blockquote -->
<!-- Inline download link -->
<a href="#" target="_blank" title="ფაილის გამოსაწერი პირველი ლინკი" class="download_inline">
	<em class="ri-file-pdf-line"></em>
	<span>ფაილის გამოსაწერი პირველი ლინკი</span>
</a>
<!-- Inline download link -->
<!-- Inline image -->
<figure class="inline_image full">
	<img src="themes/images/no_image.jpg" alt="">
</figure>
<figure class="inline_image medium left magnific">
	<a href="themes/images/no_image.jpg">
		<img src="themes/images/no_image.jpg" alt="">
	</a>
	<figcaption>Lorem ipsum dolor sit amet, consectetur adipisicing elit</figcaption>
</figure>
<figure class="inline_image right small">
	<img src="themes/images/no_image.jpg" alt="">
</figure>
<!-- Inline image -->
<!-- Inline title -->
<h2 class="inline_title">საცდელი ტექსტის საცდელი სათაური</h2>
<h3 class="inline_title">საცდელი ტექსტის საცდელი პატარა სათაური</h3>
<!-- Inline title -->