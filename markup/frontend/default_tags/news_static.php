<div class="content">
	<article class="news_static">
		<div class="featured_img magnific">
			<a href="themes/images/no_image.jpg" title="მზა ტეგები">
				<img src="themes/images/no_image.jpg" alt="მზა ტეგები">
			</a>
		</div>
		<div class="page_body">
			<h3 class="news_title">მზა ტეგები</h3>
			<div class="page_head">
				<time>
					<em class="ri-calendar-2-line"></em>
					<span>1 თვე, 2020</span>
				</time>
				<div class="share">
					<ul>
						<li>
							<a href="https://www.facebook.com/sharer/sharer.php?u=__PAGE_URL__" title="Facebook">
								<em class="ri-facebook-line"></em>
							</a>
						</li>
						<li>
							<a href="https://twitter.com/intent/tweet?text=__PAGE_TITLE__&url=__PAGE_URL__" title="Twitter">
								<em class="ri-twitter-line"></em>
							</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/shareArticle?mini=true&url=__PAGE_URL__&title=__PAGE_TITLE__&summary=&source=__PAGE_DESCRIPTION__" title="LinkedIn">
								<em class="ri-linkedin-line"></em>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="page_content">
				<p>ეს არის საცდელი ტექსტი, რომელიც მოცემულ შემთხვევაში გამოიყენება როგორც მაგალითი. მისი საშუალებით ჩვენ ვხვდებით თუ როგორი იქნება მომავალში საიტის ტექსტით დაფარული არე დიზაინერულ ასპექტში. იგი გვეხმარება უკეთ აღვიქვათ საიტის ერთიანი დიზაინი, რაც მომავალში გამორიცხავს მსგავსი პრობლემების წამოჭრას. ეს ტექსტი შესაძლოა გამოყენებულ იქნას საიტის დიზაინის შექმნის ნებისმიერ სტადიაში მაგ. საიტის დიზაინის რომელიმე გრაფიკულ პროგრამაში აწყობის დროს.</p>
				<p>იგი ასევე დაგვეხმარება ახლად შექმნილი დინამიური საიტის მართვის პანელის შესამოწმებლად. კარგად ვნახავთ თუ როგორ გამოისახება საიტზე მართვის პანელის ტექსტურ რედაქტორში (ე.წ. ედიტორში) ჩაწერის და შენახვის შემდეგ.</p>
				<p>საცდელი ტექსტი გამოსადეგია ვებ გვერდის შემქმნელის ნებისმიერი სფეროს წარმომადგენლისათვის (დიზაინერი, HTML/CSS, PHP+MySQL და ა.შ.). მათ აღარ მოუწევთ სხვა მსგავს საიტებში იდეასთან მიახლოებული ტექსტის ძებნა და მათგან დაკოპირება. ამ ტექსტის შინაარსი არის ზოგადი და შესაძლებელია გამოყენებულ იქნას ნებისმიერი შინაარსის მატარებელ საიტში.</p>
				<p>ტექსტი დაწერილია ვებსტუდია არტმედიის მიერ და მასზე საავტორო უფლებები არ არსებობს. მისი გამოყენება შეუძლია ნებისმიერ მსურველს ყოველგვარი ნებართვის გარეშე. ყველას გისურვებთ წარმატებას თქვენს საქმიანობაში.</p>
				<ul>
					<li>asdfasdfasdf</li>
					<li>asdfasdfasdf</li>
					<li>asdfasdfasdf</li>
				</ul>
			</div>
		</div>
		<div class="page_media page_photo_gallery">
			<h3>ფოტო გალერეა</h3>
			<div class="media_items magnific">
				<figure>
					<a href="themes/images/no_image.jpg">
						<img src="themes/images/no_image.jpg" alt="">
					</a>
				</figure>
				<figure>
					<a href="themes/images/no_image.jpg">
						<img src="themes/images/no_image.jpg" alt="">
					</a>
				</figure>
				<figure>
					<a href="themes/images/no_image.jpg">
						<img src="themes/images/no_image.jpg" alt="">
					</a>
				</figure>
				<figure>
					<a href="themes/images/no_image.jpg">
						<img src="themes/images/no_image.jpg" alt="">
					</a>
				</figure>
			</div>
		</div>
		<div class="page_media page_video_gallery">
			<h3>ვიდეო გალერეა</h3>
			<div class="media_items">
				<figure>
					<a href="https://www.youtube.com/watch?v=ymNFyxvIdaM" class="magnific_video">
						<img src="themes/images/no_image.jpg" alt="">
					</a>
				</figure>
				<figure>
					<a href="https://www.youtube.com/watch?v=ymNFyxvIdaM" class="magnific_video">
						<img src="themes/images/no_image.jpg" alt="">
					</a>
				</figure>
				<figure>
					<a href="https://www.youtube.com/watch?v=ymNFyxvIdaM" class="magnific_video">
						<img src="themes/images/no_image.jpg" alt="">
					</a>
				</figure>
				<figure>
					<a href="https://www.youtube.com/watch?v=ymNFyxvIdaM" class="magnific_video">
						<img src="themes/images/no_image.jpg" alt="">
					</a>
				</figure>
			</div>
		</div>
		<div class="page_media page_attachments">
			<h3>მიმაგრებული ფაილები</h3>
			<ul>
				<li>
					<a href="#" title="ფაილის დასახელების საცდელი ტექსტი" class="attachment_title" target="_blank">
						<em class="ri-file-pdf-line"></em>
						<span>ფაილის დასახელების საცდელი ტექსტიფაილის დასახელების საცდელი ტექსტიფაილის დასახელების საცდელი ტექსტი</span>
						<em class="ri ri-download-line"></em>
					</a>
				</li>
				<li>
					<a href="#" title="ფაილის დასახელების საცდელი ტექსტი" class="attachment_title" target="_blank">
						<em class="ri-file-word-line"></em>
						<span>ფაილის დასახელების საცდელი ტექსტი</span>
						<em class="ri ri-download-line"></em>
					</a>
				</li>
				<li>
					<a href="#" title="ფაილის დასახელების საცდელი ტექსტი" class="attachment_title" target="_blank">
						<em class="ri-file-excel-line"></em>
						<span>ფაილის დასახელების საცდელი ტექსტი</span>
						<em class="ri ri-download-line"></em>
					</a>
				</li>
				<li>
					<a href="#" title="ფაილის დასახელების საცდელი ტექსტი" class="attachment_title" target="_blank">
						<em class="ri-file-ppt-line"></em>
						<span>ფაილის დასახელების საცდელი ტექსტი</span>
						<em class="ri ri-download-line"></em>
					</a>
				</li>
				<li>
					<a href="#" title="ფაილის დასახელების საცდელი ტექსტი" class="attachment_title" target="_blank">
						<em class="ri-file-zip-line"></em>
						<span>ფაილის დასახელების საცდელი ტექსტი</span>
						<em class="ri ri-download-line"></em>
					</a>
				</li>
				<li>
					<a href="#" title="ფაილის დასახელების საცდელი ტექსტი" class="attachment_title" target="_blank">
						<em class="ri-file-line"></em>
						<span>ფაილის დასახელების საცდელი ტექსტი</span>
						<em class="ri ri-download-line"></em>
					</a>
				</li>
			</ul>
		</div>
	</article>
</div>