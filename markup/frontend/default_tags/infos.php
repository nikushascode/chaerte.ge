<div class="content">
	<div class="info bg_info">
		<div class="info_icon">
			<em class="ri-information-line"></em>
		</div>
		<div class="info_text">ინფორმაციის შეტყობინების საცდელი ტექსტი</div>
	</div>
	<div class="info bg_warning">
		<div class="info_icon">
			<em class="ri-error-warning-line"></em>
		</div>
		<div class="info_text">გაფრთხილების შეტყობინების საცდელი ტექსტი</div>
	</div>
	<div class="info bg_danger">
		<div class="info_icon">
			<em class="ri-close-circle-line"></em>
		</div>
		<div class="info_text">შეცდომის შეტყობინების საცდელი ტექსტი</div>
	</div>
	<div class="info bg_success">
		<div class="info_icon">
			<em class="ri-checkbox-circle-line"></em>
		</div>
		<div class="info_text">წარმატების შეტყობინების საცდელი ტექსტი</div>
	</div>
</div>