<div class="content">
	<div class="subscribe_inside">
		<div class="subscribe_desc">
			<p>ეს არის საცდელი ტექსტი, რომელიც მოცემულ შემთხვევაში გამოიყენება როგორც მაგალითი. მისი საშუალებით ჩვენ ვხვდებით თუ როგორი იქნება მომავალში საიტის ტექსტით დაფარული არე დიზაინერულ ასპექტში. იგი გვეხმარება უკეთ აღვიქვათ საიტის ერთიანი დიზაინი, რაც მომავალში გამორიცხავს მსგავსი პრობლემების წამოჭრას. ეს ტექსტი შესაძლოა გამოყენებულ იქნას საიტის დიზაინის შექმნის ნებისმიერ სტადიაში მაგ. საიტის დიზაინის რომელიმე გრაფიკულ პროგრამაში აწყობის დროს.</p>
		</div>
		<form action="subscribe.php" autocomplete="off">
			<div class="subscription_topics">
				<div class="form_item form_checkers">
					<div class="checker_wrap">
						<input type="checkbox" id="topic1" class="artform" checked="checked">
						<label for="topic1">განყოფილება 1</label>
					</div>
					<div class="checker_wrap">
						<input type="checkbox" id="topic2" class="artform">
						<label for="topic2">განყოფილება 2</label>
					</div>
					<div class="checker_wrap">
						<input type="checkbox" id="topic3" class="artform" checked="checked">
						<label for="topic3">განყოფილება 3</label>
					</div>
				</div>
			</div>
			<div class="form_body">
				<div class="form_item">
					<input type="email" placeholder="თქვენი ელფოსტა">
				</div>
				<div class="form_item form_submit">
					<button type="submit" class="gilaki">გამოწერა</button>
				</div>
			</div>
		</form>
	</div>
</div>