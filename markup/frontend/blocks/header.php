<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>მზა</title>
	<link rel="shortcut icon" href="<?php // echo base_url(); 
									?>themes/images/favicon.png">
	<!-- <meta name="theme-color" content="#000">
	<meta name="msapplication-navbutton-color" content="#000">
	<meta name="apple-mobile-web-app-status-bar-style" content="#000"> -->
	<link rel="stylesheet" href="scripts/css/main.css">
	<script type="text/javascript" src="scripts/js/main.js"></script>
	<!-- <script src="https://kit.fontawesome.com/990ba33bd4.js" crossorigin="anonymous"></script> -->
</head>

<body>

	<div class="resp_menu">
		<div class="resp_menu_toggle">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
		<div class="resp_menu_ul"></div>
	</div>

	<header class="trans-all-4">
		<div class="header_top">
			<div class="container">
				<div class="header_top_left">
					<div class="search">
						<form action="index.php" autocomplete="off">
							<input type="search" placeholder="ძიება">
							<button type="submit">
								<em class="ri-search-line"></em>
							</button>
						</form>
					</div>
					<h3>
						<em class="ri-user-follow-line"></em>
						<span>ვებგვერდი ადაპტირებულია</span>
					</h3>
				</div>
				<div class="logo">
					<a href="#" title="">
						<img src="themes/images/logo.svg" alt="">
						<span>აჭარის ავტონომიური რესპუბლიკის უმაღლესი საბჭო</span>
					</a>
				</div>
				<div class="header_top_right">
					<div class="sca">
						<a href="#" title="sca.ge">www.sca.ge</a>
					</div>
					<div class="socials">
						<ul>
							<li>
								<a href="https://facebook.com" title="Facebook" target="_blank">
									<em class="ri-facebook-line"></em>
								</a>
							</li>
							<li>
								<a href="https://instagram.com" title="Instagram" target="_blank">
									<em class="ri-instagram-line"></em>
								</a>
							</li>
							<li>
								<a href="https://youtube.com" title="YouTube" target="_blank">
									<em class="ri-youtube-line"></em>
								</a>
							</li>
						</ul>
					</div>
					<div class="live">
						<a href="#" title="Live">
							<em class="ri-live-fill"></em>
							<span>Live</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="top_nav">
			<div class="container">
				<nav class="navigation">
					<ul>
						<li>
							<a href="#" title="უმაღლესი საბჭო">უმაღლესი საბჭო</a>
						</li>
						<li>
							<a href="#" title="დაუკავშირდი დეპუტატს">დაუკავშირდი დეპუტატს</a>
						</li>
						<li>
							<a href="#" title="ჩაერთე">ჩაერთე</a>
						</li>
						<li>
							<a href="#" title="ONLINE სერვისები">ONLINE სერვისები</a>
						</li>
						<li>
							<a href="#" title="იყავი ინფორმირებული">იყავი ინფორმირებული</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</header>