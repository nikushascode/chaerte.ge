<?php include("blocks/header.php"); ?>

<div class="modes">
    <div class="dark_mode">
        <a href="#" title="">
            <em class="ri-moon-line"></em>
        </a>
    </div>
    <div class="font_size">
        <a href="#" title="">
            <em class="ri-font-size"></em>
        </a>
    </div>
</div>

<section class="main_nav trans-all-4">
    <div class="container">
        <div class="nav_items">
            <div class="nav_item">
                <a href="#" title="წარადგინე ან/და შეუერთდი პეტიციას">
                    <img src="themes/images/nav1.svg" alt="">
                    <strong>წარადგინე ან/და შეუერთდი პეტიციას</strong>
                </a>
            </div>
            <div class="nav_item">
                <a href="#" title="გააკეთე კომენტარი კანონპროექტზე">
                    <img src="themes/images/nav2.svg" alt="">
                    <strong>გააკეთე კომენტარი კანონპროექტზე</strong>
                </a>
            </div>
            <div class="nav_item">
                <a href="#" title="მიმართე უმაღლეს საბჭოს">
                    <img src="themes/images/nav3.svg" alt="">
                    <strong>მიმართე უმაღლეს საბჭოს</strong>
                </a>
            </div>
            <div class="nav_item">
                <a href="#" title="">
                    <img src="themes/images/nav4.svg" alt="დაესწარი სხდომას">
                    <strong>დაესწარი სხდომას</strong>
                </a>
            </div>
            <div class="nav_item">
                <a href="#" title="ჩანიშნე შეხვედრა დეპუტატთან">
                    <img src="themes/images/nav5.svg" alt="">
                    <strong>ჩანიშნე შეხვედრა დეპუტატთან</strong>
                </a>
            </div>
            <div class="nav_item">
                <a href="#" title="მოითხოვე ელექტრონული საშვი">
                    <img src="themes/images/nav6.svg" alt="">
                    <strong>მოითხოვე ელექტრონული საშვი</strong>
                </a>
            </div>
            <div class="nav_item">
                <a href="#" title="გაეცანი აჭარის ბიუჯეტს">
                    <img src="themes/images/nav7.svg" alt="">
                    <strong>გაეცანი აჭარის ბიუჯეტს</strong>
                </a>
            </div>
            <div class="nav_item">
                <a href="#" title="ჩაერთე საზედამხედველო საქმიანობაში">
                    <img src="themes/images/nav8.svg" alt="">
                    <strong>ჩაერთე საზედამხედველო საქმიანობაში</strong>
                </a>
            </div>
            <div class="nav_item">
                <a href="#" title="მიიღე მონაწილეობა საჯარო კონსულტაციებში">
                    <img src="themes/images/nav9.svg" alt="">
                    <strong>მიიღე მონაწილეობა საჯარო კონსულტაციებში</strong>
                </a>
            </div>
        </div>
    </div>
</section>

<div class="fixed_feedback">
    <a href="#" title="უკუკავშირი">
        <em class="ri-feedback-line"></em>
        <span>უკუკავშირი</span>
    </a>
</div>

<div class="fixed_right">
    <div class="fixed_links">
        <div class="fixed_link">
            <h3 class="fixed_title">სხდომების განრიგი</h3>
            <a href="#" title="სხდომების განრიგი">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M20.8 3.6C20.8 2.71633 20.0837 2 19.2 2H18.8V3.2C18.8 3.64182 18.4418 4 18 4H17.2C17.6418 4 18 3.64182 18 3.2V0.8C18 0.358156 17.6418 0 17.2 0C16.7582 0 16.4 0.358156 16.4 0.8V2H14.4V3.2C14.4 3.64182 14.0418 4 13.6 4H12.8C13.2418 4 13.6 3.64182 13.6 3.2V0.8C13.6 0.358156 13.2418 0 12.8 0C12.3582 0 12 0.358156 12 0.8V2H9.6V3.2C9.6 3.64182 9.24182 4 8.8 4H8C8.44182 4 8.8 3.64182 8.8 3.2V0.8C8.8 0.358156 8.44182 0 8 0C7.55818 0 7.2 0.358156 7.2 0.8V2H5.2V3.2C5.2 3.64182 4.84182 4 4.4 4H3.6C4.04182 4 4.4 3.64182 4.4 3.2V0.8C4.4 0.358156 4.04182 0 3.6 0C3.15818 0 2.8 0.358156 2.8 0.8V2H1.6C0.716332 2 0 2.71633 0 3.6V6H20.8V3.6Z" fill="#2B68B0" />
                    <path d="M19.6 15.2C17.1699 15.2 15.2 17.1699 15.2 19.6C15.2 22.03 17.1699 24 19.6 24C22.03 24 24 22.03 24 19.6C24 17.1699 22.03 15.2 19.6 15.2ZM19.6 20.4C19.4975 20.4 19.4004 20.379 19.3104 20.3439L17.85 21.5125L17.35 20.8875L18.8119 19.7182C18.8061 19.6793 18.8 19.6405 18.8 19.6C18.8 19.3046 18.9618 19.0495 19.2 18.9109V16.4H20V18.9109C20.2381 19.0495 20.4 19.3046 20.4 19.6C20.4 20.0418 20.0418 20.4 19.6 20.4Z" fill="#2B68B0" />
                    <path d="M1.6 21.5999H14.7998C14.5429 20.9841 14.4 20.3089 14.4 19.5999C14.4 17.4372 15.7209 15.5838 17.5995 14.7999H15.6V12.3999H18V14.6534C18.5044 14.4903 19.0414 14.3999 19.6 14.3999C20.0137 14.3999 20.4142 14.4533 20.8 14.5447V6.79993H0V19.9999C0 20.8839 0.715992 21.5999 1.6 21.5999ZM15.6 7.99993H18V10.3999H15.6V7.99993ZM11.2 7.99993H13.6V10.3999H11.2V7.99993ZM11.2 12.3999H13.6V14.7999H11.2V12.3999ZM11.2 16.7999H13.6V19.1999H11.2V16.7999ZM6.8 7.99993H9.2V10.3999H6.8V7.99993ZM6.8 12.3999H9.2V14.7999H6.8V12.3999ZM6.8 16.7999H9.2V19.1999H6.8V16.7999ZM2.4 7.99993H4.8V10.3999H2.4V7.99993ZM2.4 12.3999H4.8V14.7999H2.4V12.3999ZM2.4 16.7999H4.8V19.1999H2.4V16.7999Z" fill="#2B68B0" />
                </svg>
            </a>
        </div>
        <div class="fixed_link">
            <h3 class="fixed_title">საქმიანობის ანგარიში</h3>
            <a href="#" title="საქმიანობის ანგარიში">
                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M24.1071 6.25001H19.6429V0.892874C19.6429 0.399745 19.2431 0 18.75 0H0.892874C0.399745 0 0 0.399745 0 0.892874V21.4286C0 23.401 1.59898 25 3.57144 25H21.4286C23.401 25 25 23.401 25 21.4286V7.14284C25 6.64976 24.6003 6.25001 24.1071 6.25001ZM7.14284 4.46427H12.5C12.9931 4.46427 13.3928 4.86401 13.3928 5.35714C13.3928 5.85027 12.9931 6.25001 12.5 6.25001H7.14284C6.64971 6.25001 6.24996 5.85027 6.24996 5.35714C6.24996 4.86401 6.64976 4.46427 7.14284 4.46427ZM15.1785 21.4286H4.46427C3.97114 21.4286 3.57139 21.0288 3.57139 20.5357C3.57139 20.0426 3.97114 19.6428 4.46427 19.6428H15.1785C15.6717 19.6428 16.0714 20.0426 16.0714 20.5357C16.0714 21.0288 15.6717 21.4286 15.1785 21.4286ZM15.1785 17.8571H4.46427C3.97114 17.8571 3.57139 17.4574 3.57139 16.9642C3.57139 16.4711 3.97114 16.0714 4.46427 16.0714H15.1785C15.6717 16.0714 16.0714 16.4711 16.0714 16.9642C16.0714 17.4574 15.6717 17.8571 15.1785 17.8571ZM15.1785 14.2857H4.46427C3.97114 14.2857 3.57139 13.886 3.57139 13.3928C3.57139 12.8997 3.97114 12.5 4.46427 12.5H15.1785C15.6717 12.5 16.0714 12.8997 16.0714 13.3928C16.0714 13.886 15.6717 14.2857 15.1785 14.2857ZM15.1785 10.7143H4.46427C3.97114 10.7143 3.57139 10.3145 3.57139 9.8214C3.57139 9.32828 3.97114 8.92853 4.46427 8.92853H15.1785C15.6717 8.92853 16.0714 9.32828 16.0714 9.8214C16.0714 10.3145 15.6717 10.7143 15.1785 10.7143ZM23.2143 21.4286C23.2143 22.4148 22.4148 23.2143 21.4286 23.2143C20.4424 23.2143 19.6429 22.4148 19.6429 21.4286V8.03571H23.2143V21.4286H23.2143Z" fill="#2B67AF" />
                </svg>
            </a>
        </div>
    </div>
    <div class="popup_widget">
        <div class="close">
            <em class="ri-close-line"></em>
        </div>
        <div class="widget_items">
            <h3>გამოკითხვა</h3>
            <div class="widget_desc">თქვენი აზრი მნიშვნელოვანია</div>
        </div>
    </div>
</div>

<?php include("blocks/footer.php"); ?>
